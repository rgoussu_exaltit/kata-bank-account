# kata-bank-account

## Author
- Victoria Doe

## Software
- Java 17 - SpringBoot 2.7.0
- H2 Database
- JPA / Hibernate
- JSON  

## How to run the project

To run the project locally, first make sure you have the jdk 17 installed.   
Clone this [URL]("https://gitlab.com/victoria-cs-doe/kata-bank-account.git") using the command :  
```bash
    git clone url
```

You can override the following environment variables by changing their values in the application.properties file :  
| Environment Variable | Default Value |   Type    |
| -------------------- | ------------- | --------- |
| server.port          | 8080          | integer   |

## Subject
An API that allows users to perform transactions on their account.  
They can deposit and withdraw money from their accounts.  
They can see the list of transactions that were made to their account and print a statement of them.   

## Queries
1. Deposit in account
2. Withdraw from account
3. Get the list of transactions and print statement

## About
The project was constructed using hexagonal architecture and TDD.  
To be honest, TDD was not well respected as I changed the architecture of the project throughout the process of
writing the code AND I had trouble working with mockMvc  

## Project launching
The API uses a default database therefore make sure you perform all operations before restarting the server
because the restart cleans the database (for example, if you want to print a statement, perform a few deposit and
withdraw actions before)

## Refactoring  
- Ajouter le lancement des tests au CI/CD
- Ne pas squash les commits de develop vers main
- Respecter le tag 1.0.0 (par exemple)




