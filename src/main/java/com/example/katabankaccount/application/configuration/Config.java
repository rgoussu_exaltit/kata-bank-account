package com.example.katabankaccount.application.configuration;

import com.example.katabankaccount.application.port.input.DepositUseCase;
import com.example.katabankaccount.application.port.output.adapter.AccountAdapter;
import com.example.katabankaccount.application.service.AccountService;
import com.example.katabankaccount.domain.port.AccountPersistencePort;
import com.example.katabankaccount.domain.port.OperationPersistencePort;
import com.example.katabankaccount.infrastructure.adapter.persistence.mapper.AccountMapper;
import com.example.katabankaccount.infrastructure.adapter.persistence.mapper.OperationMapper;
import com.example.katabankaccount.infrastructure.adapter.persistence.repository.AccountRepository;
import com.example.katabankaccount.infrastructure.adapter.persistence.repository.OperationRepository;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@AllArgsConstructor
@Configuration
public class Config {

    private AccountRepository accountRepository;

    private OperationRepository operationRepository;

    private AccountMapper accountMapper;

    private OperationMapper operationMapper;

    @Bean
    public AccountPersistencePort accountPersistence() {
        return new AccountAdapter(accountRepository, operationRepository);
    }

    @Bean
    public OperationPersistencePort operationPersistence() {
        return new AccountAdapter(accountRepository, operationRepository);
    }

    @Bean
    public DepositUseCase depositUseCase(){
        return new AccountService(accountPersistence(), operationPersistence(), accountMapper, operationMapper);
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
