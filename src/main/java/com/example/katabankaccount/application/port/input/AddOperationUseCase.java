package com.example.katabankaccount.application.port.input;

import com.example.katabankaccount.domain.model.Money;
import com.example.katabankaccount.domain.model.OperationType;

public interface AddOperationUseCase {

    void addOperation(Long accountId, Money money, OperationType operationType);

}
