package com.example.katabankaccount.application.port.input;

import com.example.katabankaccount.domain.model.Account;
import com.example.katabankaccount.domain.model.Money;

public interface DepositUseCase {

    Account deposit(Long id, Money money);
}
