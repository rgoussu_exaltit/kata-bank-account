package com.example.katabankaccount.application.port.input;

import com.example.katabankaccount.domain.model.Operation;

import java.util.List;

public interface ListOperationsUseCase {

    List<Operation> listOperations(Long id);
}
