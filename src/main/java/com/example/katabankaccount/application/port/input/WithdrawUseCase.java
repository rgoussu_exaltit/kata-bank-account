package com.example.katabankaccount.application.port.input;

import com.example.katabankaccount.domain.model.Account;
import com.example.katabankaccount.domain.model.Money;

public interface WithdrawUseCase {

    Account withdraw(Long id, Money money);
}
