package com.example.katabankaccount.application.rest;

import com.example.katabankaccount.application.port.input.DepositUseCase;
import com.example.katabankaccount.application.port.input.ListOperationsUseCase;
import com.example.katabankaccount.application.port.input.WithdrawUseCase;
import com.example.katabankaccount.domain.model.Account;
import com.example.katabankaccount.domain.model.Money;
import com.example.katabankaccount.domain.model.Operation;
import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/accounts")
public class AccountController {

    private DepositUseCase depositUseCase;

    private WithdrawUseCase withdrawUseCase;

    private ListOperationsUseCase listOperationsUseCase;

    @PutMapping("/deposit/{accountId}")
    @ResponseStatus(value = HttpStatus.OK)
    public Account depositMoney(@PathVariable @NotNull Long accountId, @RequestBody @NotNull Money money) {
        return depositUseCase.deposit(accountId, money);
    }

    @PutMapping("/withdraw/{accountId}")
    @ResponseStatus(value = HttpStatus.OK)
    public Account withdrawMoney(@PathVariable @NotNull Long accountId, @RequestBody @NotNull Money money) {
        return withdrawUseCase.withdraw(accountId, money);
    }

    @GetMapping("/print/{accountId}")
    @ResponseStatus(value = HttpStatus.OK)
    public List<Operation> printOperations(@PathVariable @NotNull Long accountId) {
        return listOperationsUseCase.listOperations(accountId);
    }
}
