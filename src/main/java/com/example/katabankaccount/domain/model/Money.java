package com.example.katabankaccount.domain.model;

public class Money {

    public Money() {}

    public Money(double amount){
        this.amount = amount;
    }

    private double amount;

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public boolean isPositive(){
        return amount > 0;
    }
}
