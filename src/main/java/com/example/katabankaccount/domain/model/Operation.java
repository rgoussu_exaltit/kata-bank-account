package com.example.katabankaccount.domain.model;

import java.util.Date;

public class Operation {

    public Operation(){}

    public Operation(Long operationId, OperationType operationType, Date date, double amount, double balance) {
        this.operationId = operationId;
        this.operationType = operationType;
        this.date = date;
        this.amount = amount;
        this.balance = balance;
    }

    private Long operationId;

    private OperationType operationType;

    private Date date;

    private double amount;

    private double balance;

    public Long getOperationId() {
        return operationId;
    }

    public void setOperationId(Long operationId) {
        this.operationId = operationId;
    }

    public OperationType getOperationType() {
        return operationType;
    }

    public void setOperationType(OperationType operationType) {
        this.operationType = operationType;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }
}
