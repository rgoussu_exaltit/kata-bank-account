package com.example.katabankaccount.domain.model;

public enum OperationType {
    DEPOSIT, WITHDRAW
}
