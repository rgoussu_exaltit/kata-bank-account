package com.example.katabankaccount.domain.port;

import com.example.katabankaccount.domain.model.Money;
import com.example.katabankaccount.infrastructure.adapter.persistence.entity.AccountEntity;

public interface AccountPersistencePort {

    AccountEntity deposit(Long accountId, Money money);
    AccountEntity withdraw(Long accountId, Money money);

}
