package com.example.katabankaccount.domain.port;

import com.example.katabankaccount.domain.model.Money;
import com.example.katabankaccount.domain.model.OperationType;
import com.example.katabankaccount.infrastructure.adapter.persistence.entity.OperationEntity;

import java.util.List;

public interface OperationPersistencePort {

    List<OperationEntity> listOperations(Long accountId);
    void addOperation(Long accountId, Money money, OperationType operationType);
}
