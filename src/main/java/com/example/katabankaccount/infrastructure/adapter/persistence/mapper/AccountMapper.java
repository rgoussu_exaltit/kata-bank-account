package com.example.katabankaccount.infrastructure.adapter.persistence.mapper;

import com.example.katabankaccount.domain.model.Account;
import com.example.katabankaccount.infrastructure.adapter.persistence.entity.AccountEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AccountMapper {

    Account map(AccountEntity account);

}
