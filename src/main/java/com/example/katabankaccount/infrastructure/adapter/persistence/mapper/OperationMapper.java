package com.example.katabankaccount.infrastructure.adapter.persistence.mapper;

import com.example.katabankaccount.domain.model.Operation;
import com.example.katabankaccount.infrastructure.adapter.persistence.entity.OperationEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface OperationMapper {

    List<Operation> map(List<OperationEntity> operation);
    Operation map(OperationEntity operationEntity);
}
