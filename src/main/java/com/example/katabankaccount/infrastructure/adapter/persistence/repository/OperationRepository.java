package com.example.katabankaccount.infrastructure.adapter.persistence.repository;

import com.example.katabankaccount.infrastructure.adapter.persistence.entity.OperationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OperationRepository extends JpaRepository<OperationEntity, Long> {

    List<OperationEntity> findByAccountId(Long accountId);
}
