package com.example.katabankaccount;

import com.example.katabankaccount.application.rest.AccountController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class KataBankAccountApplicationTests {

    @Autowired
    private AccountController accountController;

    @Test
    void contextLoads() {
        assertNotNull(accountController);
    }

}
